describe('Chat', function () {
  
  describe('Sending messages', function() {
        
        it('should be able send a message', function () {
            expect(sendMessage('Jasmine test')).toBe(true);
        });
    
        it('should ignore blank messages', function () {
            expect(sendMessage('')).toBe(false);
        });
        
        it('should require a string parameter', function() {
            expect(sendMessage()).toBe(false);
        });
    });
    
});