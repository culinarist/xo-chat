describe('Login and Registration', function () {
  
  describe('Registering', function() {
        
        it('should be able to register', function () {
            expect(notes.add('sixth note')).toBe(true);
            expect(notes.count()).toBe(6);
        });
    
        it('should ignore blank notes', function () {
            expect(notes.add('')).toBe(false);
            expect(notes.count()).toBe(5);
        });
    
        it('should ignore notes containing only whitespace', function() {
            expect(notes.add('   ')).toBe(false);
            expect(notes.count()).toBe(5);
        });
        
        it('should require a string parameter', function() {
            expect(notes.add()).toBe(false);
            expect(notes.count()).toBe(5);
        });
    });
    
    describe('deleting a note', function() {
        it('should be able to delete the first index', function () {
            expect(notes.remove(0)).toBe(true);
            expect(notes.count()).toBe(4);
        });
        it('should be able to delete the last index', function () {
            expect(notes.remove(4)).toBe(true);
            expect(notes.count()).toBe(4);
        });
        it('should return false if index is out of range', function () {
            expect(notes.remove(12)).toBe(false);
            expect(notes.count()).toBe(5);
        });
        it('should return false if the parameter is missing', function () {
            expect(notes.remove( )).toBe(false);
            expect(notes.count()).toBe(5);
        });
    }); 
    
    describe('finding a note', function() {
        it('should be able to find note', function () {
            expect(notes.find('note')).toBe(true);
        });
        it('should not be able to find Note', function () {
            expect(notes.find('Note')).toBe(false);
        });
        it('should be able to find th', function () {
            expect(notes.find('th')).toBe(true);
        });
        it('should be able to find four', function () {
            expect(notes.find('four')).toBe(true);
        });
        it('should not be able to find blank string', function () {
            expect(notes.find('')).toBe(false);
        });
    });
});