<?php

include('login.php'); // Includes Login Script
include('register.php');

if(isset($_SESSION['login_user'])){
header("location: chat.php");
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Chat Sign in</title>

    <!-- Bootstrap CSS -->
    <link href="bootstrap.min.css" rel="stylesheet">
    
    <link href="signin.css" rel="stylesheet">
    
  </head>

  <body>
    <div class="container" id="loginpage">

      <form class="form-signin" id="log" method="post" >
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" id="inputUsername" name="username" class="form-control" placeholder="Username" required>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <a href="#register">or register</a>
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit" id="logbutton">Sign in</button>
        <span><?php echo $loginerror; ?></span>
      </form>

    </div> <!-- /container -->
    
    <div class="container" id="registerpage">
        
      <form class="form-signin" id="reg" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <h2 class="form-signin-heading">Register</h2>
        <label for="inputUsernameReg" class="sr-only">Username</label>
        <input type="text" name="rusername" id="inputUsernameReg"  class="form-control" placeholder="Username" required>
        <label for="inputPasswordReg" class="sr-only">Password</label>
        <input type="password" name="rpassword" id="inputPasswordReg"  class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <a href="#login">or sign in</a>
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit" name="regsubmit" id="regbutton">Register</button>
        <span><?php echo $registererror; ?></span>
      </form>

    </div>
    
  <script src="login.js"></script>
</body>

</html>