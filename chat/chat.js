/*global $*/

/**
 * Function which runs when the chat HTML page is loaded.
 * It loads the messages, users that are online and sets the messages box scrollbar down.
 */
window.onload = function(){ 
	getMessages();
	getUsersOnline();
	//setScrollbarDown();
};

/**
 * JQuery function which triggers the 'send message' button when enter is pressed.
 */
$(document).ready(function(){
    $('#msg').keypress(function(e){
      if(e.keyCode==13)
      $('#sendmessage').click();
    });
});

/**
 * Function which is called when sending a message.
 * It posts the message and its sender to the REST API with XMLHttpRequest.
 * @param {string} msg - The message that was written.
 */
function sendMessage(msg) {
    document.getElementById("msg").value = "";
	var username;
	
	/**
    * Function which gets the sender of the message from sender.php.
    */
	function getSender() {
        var xhttp  = new XMLHttpRequest();
        xhttp.open("GET", "sender.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send();
    
        xhttp.onreadystatechange = function() {
    	    if (xhttp.readyState == 4 && xhttp.status == 200) {
        	    username = xhttp.responseText;
        	    
        	    var message = msg;
	            var params = "username="+username+"&message="+message;
	            
	            xhttp.open("POST", "https://xoapi-culinarist.c9users.io/xorestapi/CodeIgniter-3.0.0/index.php/XOapi/message", true);
	            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	
	            xhttp.send(params);
	            
	            xhttp.onreadystatechange = function() {
    	            if (xhttp.readyState == 4 && xhttp.status == 200) {
        	            getMessages();
    	            }
	            };
    	    }
	    };
    }
	
	getSender();
}

/**
 * Function which sets the message box scrollbar down.
 */
function setScrollbarDown() {
    var textarea = document.getElementById('messagesdiv');
    textarea.scrollTop = textarea.scrollHeight;
}

/**
 * Function which sends an XMLHttpRequest to the REST API to get all the messages in the database
 * and then calls the printMessages function and gives it all the messages in the parameter.
 */
function getMessages() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "https://xoapi-culinarist.c9users.io/xorestapi/CodeIgniter-3.0.0/index.php/XOapi/messages", true);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var txt = JSON.parse(xmlhttp.responseText);
            var content = '{ "messages" : ' + txt + '}';
            var msg = JSON.parse(content);
            printMessages(msg);
        }
    };
}

/**
 * Function which prints out all the messages to the messages box.
 * It gets all the messages in a parameter and then makes HTML5 list items that contain the messages.
 * @param {object} msg - Contains all the messages.
 */
function printMessages(msg) {
    var size = 0;
    for (var i in msg.messages) {
        if (msg.messages.hasOwnProperty(i)) {
            size++;
        }
    }
    var str = "";
    for (var i = 0; i < size; i++){
        if (i == 0) {
            str = '<li class="media"><div class="media-body"><div class="media"><a class="pull-left" href="#"></a><div class="media-body" >' + msg.messages[i].message + '<br /><small class="text-muted">' + msg.messages[i].sender + ' | <span id="time">' + msg.messages[i].time + '</span></small><hr /></div></div></div></li>';
        } else {
            str = str + '<li class="media"><div class="media-body"><div class="media"><a class="pull-left" href="#"></a><div class="media-body" >' + msg.messages[i].message + '<br /><small class="text-muted">' + msg.messages[i].sender + ' | <span id="time">' + msg.messages[i].time + '</span></small><hr /></div></div></div></li>';
        }
        
    }
    document.getElementById("messages").innerHTML = str;
    
    setScrollbarDown();
}

/**
 * Function which sends an XMLHttpRequest to the REST API to get all the users online
 * and then calls the printUsers function and gives it all the users in the parameter.
 */
function getUsersOnline() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "https://xoapi-culinarist.c9users.io/xorestapi/CodeIgniter-3.0.0/index.php/XOapi/usersonline", true);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var txt = JSON.parse(xmlhttp.responseText);
            var content = '{ "users" : ' + txt + '}';
            var userlist = JSON.parse(content);
            printUsers(userlist);
        }
    };
}

/**
 * Function which prints out all the users that are online to the users list.
 * It gets all the users online in a parameter and then makes HTML5 list items that contain the users.
 * @param {object} userlist - Contains users that are online.
 */
function printUsers(userlist) {
    var size = 0;
    for (var i in userlist.users) {
        if (userlist.users.hasOwnProperty(i)) {
            size++;
        }
    }
    
    for (var i = 0; i < size; i++){
        if (i == 0) {
            var str = '<li class="media"><div class="media-body"><div class="media"><div class="media-body" ><h5>' + userlist.users[i].username + ' | User </h5><small class="text-muted">Active</small></div></div></div></li>';
        } else {
            str = str + '<li class="media"><div class="media-body"><div class="media"><div class="media-body" ><h5>' + userlist.users[i].username + ' | User </h5><small class="text-muted">Active</small></div></div></div></li>';
        }
    }
    
    document.getElementById("users").innerHTML = str;
    
}

/**
 * Function which is called every second to update the message box and active users list.
 */
setInterval(function() {
    getMessages();
    getUsersOnline();
}, 1000);