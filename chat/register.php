<?php
$registererror=''; // Variable To Store Error Message

$username = $password = "";
$usernameok = FALSE;
$passwordok = FALSE;

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if (isset($_POST['regsubmit'])) {
    if (empty($_POST['rusername']) || empty($_POST['rpassword'])) {
        $registererror = "Username or Password is invalid";
    }else{

        $username = test_input($_POST['rusername']);
        if (!preg_match('/^[A-Za-z0-9]{3,12}$/',$username)) {
            
            $message = 'Only letters and numbers allowed in username.';

                echo "<SCRIPT type='text/javascript'>
                alert('$message');
                window.location.replace(\"https://ristinolla-tonzaw.c9users.io/Ristinolla/chat/index.php#register\");
                </SCRIPT>";
        }else{
            $usernameok = TRUE;
        }
        
        $password = test_input($_POST['rpassword']);
        if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z]{5,18}$/',$password)) {
            
            $message = 'Password must contain atleast 5 characters, 1 number and 1 letter.';

                echo "<SCRIPT type='text/javascript'>
                alert('$message');
                window.location.replace(\"https://ristinolla-tonzaw.c9users.io/Ristinolla/chat/index.php#register\");
                </SCRIPT>";
        }else{
            $passwordok = TRUE;
        }
        
        if($usernameok === TRUE && $passwordok === TRUE){
            $password=md5($password);
            
            $url = 'https://xoapi-culinarist.c9users.io/xorestapi/CodeIgniter-3.0.0/index.php/XOapi/user';
            $data = array('username' => $username, 'password' => $password);

            $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
        
            if ($result === 'false') { 
                $registererror = "Please try another username.";
            }else if($result === 'true'){
                $message = 'Registeration succeeded.';

                echo "<SCRIPT type='text/javascript'>
                alert('$message');
                window.location.replace(\"https://ristinolla-tonzaw.c9users.io/Ristinolla/chat/index.php#login\");
                </SCRIPT>";
            }
        }
    }
}
    
?>
