/**
 * Function which runs when the login HTML page is loaded.
 * It loads the loads either the login page or the register page.
 */
window.onload = function(){ 
	if (window.location.hash == '#register') {
		document.getElementById('loginpage').style.display = 'none';
		
	} else {
	    document.getElementById('registerpage').style.display = 'none';
	}
};

/**
 * Function which is called when the 'or register' link is clicked.
 * It changes the sign in form to the registration form.
 */
document.querySelector('#registerpage a').onclick = function() {
	    document.getElementById('registerpage').style.display = 'none';
	    document.getElementById('loginpage').style.display = 'inherit';
};

/**
 * Function which is called when the 'or sign in' link is clicked.
 * It changes the registration form to the sign in form.
 */
document.querySelector('#loginpage a').onclick = function() {
	    document.getElementById('loginpage').style.display = 'none';
	    document.getElementById('registerpage').style.display = 'inherit';
};