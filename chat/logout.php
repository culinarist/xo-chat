<?php

session_start();

        $username=$_SESSION['login_user'];
        
        $url = 'https://xoapi-culinarist.c9users.io/xorestapi/CodeIgniter-3.0.0/index.php/XOapi/userlogout';
        $data = array('username' => $username);

        $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        
        if($result == 'true'){
            if(session_destroy()) // Destroying All Sessions
            {
            header("Location: index.php"); // Redirecting To Home Page
            }
        }
?>