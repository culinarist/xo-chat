<?php
include('session.php');
if(!isset($_SESSION['login_user'])){
header("location: index.php");
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    
    <title>Chat</title>
    
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="bootstrapchat.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body style="font-family:Verdana">
<div class="container">
    <h1 class="text-center" > CHAT </h1>
    <div id="topbar">
        <p>Welcome, <b><?php echo $_SESSION['login_user']; ?></b></p>
        <a id="logout" href="logout.php" class="btn btn-info btn-lg">
            <span class="glyphicon glyphicon-log-out"></span> Log out
        </a>
    </div>
    <div class="row " style="padding-top:20px;">
        <br /><br />
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    RECENT CHAT HISTORY
                </div>
                <div id="messagesdiv" class="panel-body">
                    <ul id="messages" class="media-list">
    
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="msg" type="text" class="form-control" placeholder="Enter Message" />
                        <span class="input-group-btn">
                            <button id="sendmessage" class="btn btn-info" type="submit" onClick="sendMessage(document.getElementById('msg').value);">SEND</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                   USERS ONLINE
                </div>
                <div class="panel-body">
                    <ul id="users" class="media-list">
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="chat.js"></script>
</body>
</html>
