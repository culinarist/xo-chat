<?php
session_start(); // Starting Session
$loginerror=''; // Variable To Store Error Message

$usernameok = FALSE;
$passwordok = FALSE;

function test_loginput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

if (isset($_POST['submit'])) {
    if (empty($_POST['username']) || empty($_POST['password'])) {
        $loginerror = "Username or Password is invalid";
    }else{
        
        $username = test_loginput($_POST['username']);
        
        $password = test_loginput($_POST['password']);
        
            $password = md5($password);
            $url = 'https://xoapi-culinarist.c9users.io/xorestapi/CodeIgniter-3.0.0/index.php/XOapi/userlog';
            $data = array('username' => $username, 'password' => $password);

            $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === 'false') { 
                $loginerror = "Username or Password is invalid";
            }else if($result === 'true'){
                $_SESSION['login_user']=$username; // Initializing Session
                header("location: chat/chat.php"); // Redirecting To Other Page
            }
    }
}
?>